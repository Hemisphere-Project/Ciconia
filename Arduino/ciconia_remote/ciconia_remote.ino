/*
   SETTINGS
*/
#define CR_VERSION  0.01  // Init

/*
   INCLUDES
*/
#include "debug.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include "SSD1306.h"

HTTPClient http;

// TTGO
uint8_t ledPin = 16; // Onboard LED reference
SSD1306 display(0x3c, 5, 4); // instance for the OLED. Addr, SDA, SCL


/*
   SETUP
*/
void setup() {

  LOGSETUP();

  // Wifi
  wifi_static("3.0.0.100");
  //wifi_connect("interweb", "superspeed37");
  wifi_connect("ciconia");
  wifi_ota( "ciconia-remote v" + String(CR_VERSION, 2) );
  wifi_onConnect(doOnConnect);

  // Init Screen
  display.init(); // initialise the OLED
  display.flipScreenVertically(); // does what is says
  display.setFont(ArialMT_Plain_24); // does what is says
  // Set the origin of text to top left
  display.setTextAlignment(TEXT_ALIGN_LEFT);

  display.clear(); // clear the display
  // prep a string in the screen buffer
  display.drawString(0, 0, "Ciconia");
  display.drawString(10, 24, "Ciconia");
  display.display(); // display whatever is in the buffer

}

/*
   LOOP
*/
void loop() {

}


/*
   on Connect
*/
void doOnConnect() {
  httpGet("/loop");
  httpGet("/play/simpsons.mp4");
}

String httpGet(String url) {
  http.begin("http://3.0.0.1:8037" + url);
  int httpCode = http.GET();
  String payload = "";
  if (httpCode > 0) { //Check for the returning code
    payload = http.getString();
  } else {
    payload = "ERROR "+String(httpCode);
    LOG("Error on HTTP request: " + String(httpCode));
  }
  http.end();
  LOG(payload);
  return payload;
}

